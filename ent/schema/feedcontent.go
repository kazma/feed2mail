package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
)

// FeedContent holds the schema definition for the FeedContent entity.
type FeedContent struct {
	ent.Schema
}

// Fields of the FeedContent.
func (FeedContent) Fields() []ent.Field {
	return []ent.Field{
		field.Int("id"),
		field.String("feed_name"),
		field.String("link"),
		field.String("title"),
		field.String("detail"),
		field.Time("create_time"),
		field.String("at_day"),
		field.Time("published_time").Nillable(),
	}
}

func (FeedContent) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("feed_name", "link"),
		index.Fields("feed_name", "at_day", "published_time"),
		index.Fields("create_time"),
	}
}

// Edges of the FeedContent.
func (FeedContent) Edges() []ent.Edge {
	return nil
}
