package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
)

// Feeds holds the schema definition for the Feeds entity.
type Feeds struct {
	ent.Schema
}

// Fields of the Feeds.
func (Feeds) Fields() []ent.Field {
	return []ent.Field{
		field.Int("id"),
		field.String("feed_name"),
		field.String("feed_url"),
		field.Int("feed_weight"),
		field.Time("create_time").Default(time.Now()),
		field.Time("new_content_time"),
	}
}

func (Feeds) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("feed_name"),
	}
}

// Edges of the Feeds.
func (Feeds) Edges() []ent.Edge {
	return nil
}
