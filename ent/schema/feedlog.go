package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
)

// FeedLog holds the schema definition for the FeedLog entity.
type FeedLog struct {
	ent.Schema
}

// Fields of the FeedLog.
func (FeedLog) Fields() []ent.Field {
	return []ent.Field{
		field.Int("id"),
		field.String("content"),
		field.Time("create_time"),
		field.Enum("log_type").Values("ALL", "DEBUG", "ERROR"),
	}
}

func (FeedLog) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("create_time"),
	}
}

// Edges of the FeedLog.
func (FeedLog) Edges() []ent.Edge {
	return nil
}
