package main

import (
	"context"
	"github.com/pkg/errors"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common"
	tcerr "github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/errors"
	tchttp "github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/http"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/json"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/profile"
)

const APIVersion = "2018-03-21"

// Predefined struct for user
type TextTranslateBatchRequestParams struct {
	// 源语言，支持：
	// auto：自动识别（识别为一种语言）
	// zh：简体中文
	// zh-TW：繁体中文
	// en：英语
	// ja：日语
	// ko：韩语
	// fr：法语
	// es：西班牙语
	// it：意大利语
	// de：德语
	// tr：土耳其语
	// ru：俄语
	// pt：葡萄牙语
	// vi：越南语
	// id：印尼语
	// th：泰语
	// ms：马来西亚语
	// ar：阿拉伯语
	// hi：印地语
	Source *string `json:"Source,omitnil,omitempty" name:"Source"`

	// 目标语言，各源语言的目标语言支持列表如下
	//
	// <li> zh（简体中文）：en（英语）、ja（日语）、ko（韩语）、fr（法语）、es（西班牙语）、it（意大利语）、de（德语）、tr（土耳其语）、ru（俄语）、pt（葡萄牙语）、vi（越南语）、id（印尼语）、th（泰语）、ms（马来语）</li>
	// <li>zh-TW（繁体中文）：en（英语）、ja（日语）、ko（韩语）、fr（法语）、es（西班牙语）、it（意大利语）、de（德语）、tr（土耳其语）、ru（俄语）、pt（葡萄牙语）、vi（越南语）、id（印尼语）、th（泰语）、ms（马来语）</li>
	// <li>en（英语）：zh（中文）、zh-TW（繁体中文）、ja（日语）、ko（韩语）、fr（法语）、es（西班牙语）、it（意大利语）、de（德语）、tr（土耳其语）、ru（俄语）、pt（葡萄牙语）、vi（越南语）、id（印尼语）、th（泰语）、ms（马来语）、ar（阿拉伯语）、hi（印地语）</li>
	// <li>ja（日语）：zh（中文）、zh-TW（繁体中文）、en（英语）、ko（韩语）</li>
	// <li>ko（韩语）：zh（中文）、zh-TW（繁体中文）、en（英语）、ja（日语）</li>
	// <li>fr（法语）：zh（中文）、zh-TW（繁体中文）、en（英语）、es（西班牙语）、it（意大利语）、de（德语）、tr（土耳其语）、ru（俄语）、pt（葡萄牙语）</li>
	// <li>es（西班牙语）：zh（中文）、zh-TW（繁体中文）、en（英语）、fr（法语）、it（意大利语）、de（德语）、tr（土耳其语）、ru（俄语）、pt（葡萄牙语）</li>
	// <li>it（意大利语）：zh（中文）、zh-TW（繁体中文）、en（英语）、fr（法语）、es（西班牙语）、de（德语）、tr（土耳其语）、ru（俄语）、pt（葡萄牙语）</li>
	// <li>de（德语）：zh（中文）、zh-TW（繁体中文）、en（英语）、fr（法语）、es（西班牙语）、it（意大利语）、tr（土耳其语）、ru（俄语）、pt（葡萄牙语）</li>
	// <li>tr（土耳其语）：zh（中文）、zh-TW（繁体中文）、en（英语）、fr（法语）、es（西班牙语）、it（意大利语）、de（德语）、ru（俄语）、pt（葡萄牙语）</li>
	// <li>ru（俄语）：zh（中文）、zh-TW（繁体中文）、en（英语）、fr（法语）、es（西班牙语）、it（意大利语）、de（德语）、tr（土耳其语）、pt（葡萄牙语）</li>
	// <li>pt（葡萄牙语）：zh（中文）、zh-TW（繁体中文）、en（英语）、fr（法语）、es（西班牙语）、it（意大利语）、de（德语）、tr（土耳其语）、ru（俄语）</li>
	// <li>vi（越南语）：zh（中文）、zh-TW（繁体中文）、en（英语）</li>
	// <li>id（印尼语）：zh（中文）、zh-TW（繁体中文）、en（英语）</li>
	// <li>th（泰语）：zh（中文）、zh-TW（繁体中文）、en（英语）</li>
	// <li>ms（马来语）：zh（中文）、zh-TW（繁体中文）、en（英语）</li>
	// <li>ar（阿拉伯语）：en（英语）</li>
	// <li>hi（印地语）：en（英语）</li>
	Target *string `json:"Target,omitnil,omitempty" name:"Target"`

	// 项目ID，可以根据控制台-账号中心-项目管理中的配置填写，如无配置请填写默认项目ID:0
	ProjectId *int64 `json:"ProjectId,omitnil,omitempty" name:"ProjectId"`

	// 待翻译的文本列表，批量接口可以以数组方式在一次请求中填写多个待翻译文本。文本统一使用utf-8格式编码，非utf-8格式编码字符会翻译失败，请传入有效文本，html标记等非常规翻译文本可能会翻译失败。单次请求的文本长度总和需要低于6000字符。
	SourceTextList []*string `json:"SourceTextList,omitnil,omitempty" name:"SourceTextList"`
}

type TextTranslateBatchRequest struct {
	*tchttp.BaseRequest

	// 源语言，支持：
	// auto：自动识别（识别为一种语言）
	// zh：简体中文
	// zh-TW：繁体中文
	// en：英语
	// ja：日语
	// ko：韩语
	// fr：法语
	// es：西班牙语
	// it：意大利语
	// de：德语
	// tr：土耳其语
	// ru：俄语
	// pt：葡萄牙语
	// vi：越南语
	// id：印尼语
	// th：泰语
	// ms：马来西亚语
	// ar：阿拉伯语
	// hi：印地语
	Source *string `json:"Source,omitnil,omitempty" name:"Source"`

	// 目标语言，各源语言的目标语言支持列表如下
	//
	// <li> zh（简体中文）：en（英语）、ja（日语）、ko（韩语）、fr（法语）、es（西班牙语）、it（意大利语）、de（德语）、tr（土耳其语）、ru（俄语）、pt（葡萄牙语）、vi（越南语）、id（印尼语）、th（泰语）、ms（马来语）</li>
	// <li>zh-TW（繁体中文）：en（英语）、ja（日语）、ko（韩语）、fr（法语）、es（西班牙语）、it（意大利语）、de（德语）、tr（土耳其语）、ru（俄语）、pt（葡萄牙语）、vi（越南语）、id（印尼语）、th（泰语）、ms（马来语）</li>
	// <li>en（英语）：zh（中文）、zh-TW（繁体中文）、ja（日语）、ko（韩语）、fr（法语）、es（西班牙语）、it（意大利语）、de（德语）、tr（土耳其语）、ru（俄语）、pt（葡萄牙语）、vi（越南语）、id（印尼语）、th（泰语）、ms（马来语）、ar（阿拉伯语）、hi（印地语）</li>
	// <li>ja（日语）：zh（中文）、zh-TW（繁体中文）、en（英语）、ko（韩语）</li>
	// <li>ko（韩语）：zh（中文）、zh-TW（繁体中文）、en（英语）、ja（日语）</li>
	// <li>fr（法语）：zh（中文）、zh-TW（繁体中文）、en（英语）、es（西班牙语）、it（意大利语）、de（德语）、tr（土耳其语）、ru（俄语）、pt（葡萄牙语）</li>
	// <li>es（西班牙语）：zh（中文）、zh-TW（繁体中文）、en（英语）、fr（法语）、it（意大利语）、de（德语）、tr（土耳其语）、ru（俄语）、pt（葡萄牙语）</li>
	// <li>it（意大利语）：zh（中文）、zh-TW（繁体中文）、en（英语）、fr（法语）、es（西班牙语）、de（德语）、tr（土耳其语）、ru（俄语）、pt（葡萄牙语）</li>
	// <li>de（德语）：zh（中文）、zh-TW（繁体中文）、en（英语）、fr（法语）、es（西班牙语）、it（意大利语）、tr（土耳其语）、ru（俄语）、pt（葡萄牙语）</li>
	// <li>tr（土耳其语）：zh（中文）、zh-TW（繁体中文）、en（英语）、fr（法语）、es（西班牙语）、it（意大利语）、de（德语）、ru（俄语）、pt（葡萄牙语）</li>
	// <li>ru（俄语）：zh（中文）、zh-TW（繁体中文）、en（英语）、fr（法语）、es（西班牙语）、it（意大利语）、de（德语）、tr（土耳其语）、pt（葡萄牙语）</li>
	// <li>pt（葡萄牙语）：zh（中文）、zh-TW（繁体中文）、en（英语）、fr（法语）、es（西班牙语）、it（意大利语）、de（德语）、tr（土耳其语）、ru（俄语）</li>
	// <li>vi（越南语）：zh（中文）、zh-TW（繁体中文）、en（英语）</li>
	// <li>id（印尼语）：zh（中文）、zh-TW（繁体中文）、en（英语）</li>
	// <li>th（泰语）：zh（中文）、zh-TW（繁体中文）、en（英语）</li>
	// <li>ms（马来语）：zh（中文）、zh-TW（繁体中文）、en（英语）</li>
	// <li>ar（阿拉伯语）：en（英语）</li>
	// <li>hi（印地语）：en（英语）</li>
	Target *string `json:"Target,omitnil,omitempty" name:"Target"`

	// 项目ID，可以根据控制台-账号中心-项目管理中的配置填写，如无配置请填写默认项目ID:0
	ProjectId *int64 `json:"ProjectId,omitnil,omitempty" name:"ProjectId"`

	// 待翻译的文本列表，批量接口可以以数组方式在一次请求中填写多个待翻译文本。文本统一使用utf-8格式编码，非utf-8格式编码字符会翻译失败，请传入有效文本，html标记等非常规翻译文本可能会翻译失败。单次请求的文本长度总和需要低于6000字符。
	SourceTextList []*string `json:"SourceTextList,omitnil,omitempty" name:"SourceTextList"`
}

var (
	TencentClient *Client
)

func InitTencentClient() {
	_client, err := NewClient(
		common.NewCredential(GlobalConfig.Tencent.SecretId, GlobalConfig.Tencent.SecretKey),
		"ap-guangzhou",
		profile.NewClientProfile(),
	)
	if err != nil {
		panic(err)
	}
	TencentClient = _client
}

func (r *TextTranslateBatchRequest) ToJsonString() string {
	b, _ := json.Marshal(r)
	return string(b)
}

// FromJsonString It is highly **NOT** recommended to use this function
// because it has no param check, nor strict type check
func (r *TextTranslateBatchRequest) FromJsonString(s string) error {
	f := make(map[string]interface{})
	if err := json.Unmarshal([]byte(s), &f); err != nil {
		return err
	}
	delete(f, "Source")
	delete(f, "Target")
	delete(f, "ProjectId")
	delete(f, "SourceTextList")
	if len(f) > 0 {
		return tcerr.NewTencentCloudSDKError("ClientError.BuildRequestError", "TextTranslateBatchRequest has unknown keys!", "")
	}
	return json.Unmarshal([]byte(s), &r)
}

// Predefined struct for user
type TextTranslateBatchResponseParams struct {
	// 源语言，详见入参Source
	Source *string `json:"Source,omitnil,omitempty" name:"Source"`

	// 目标语言，详见入参Target
	Target *string `json:"Target,omitnil,omitempty" name:"Target"`

	// 翻译后的文本列表
	TargetTextList []*string `json:"TargetTextList,omitnil,omitempty" name:"TargetTextList"`

	// 唯一请求 ID，每次请求都会返回。定位问题时需要提供该次请求的 RequestId。
	RequestId *string `json:"RequestId,omitnil,omitempty" name:"RequestId"`
}

type TextTranslateBatchResponse struct {
	*tchttp.BaseResponse
	Response *TextTranslateBatchResponseParams `json:"Response"`
}

func (r *TextTranslateBatchResponse) ToJsonString() string {
	b, _ := json.Marshal(r)
	return string(b)
}

// FromJsonString It is highly **NOT** recommended to use this function
// because it has no param check, nor strict type check
func (r *TextTranslateBatchResponse) FromJsonString(s string) error {
	return json.Unmarshal([]byte(s), &r)
}

type Client struct {
	common.Client
}

func NewTextTranslateBatchRequest() (request *TextTranslateBatchRequest) {
	request = &TextTranslateBatchRequest{
		BaseRequest: &tchttp.BaseRequest{},
	}

	request.Init().WithApiInfo("tmt", APIVersion, "TextTranslateBatch")

	return
}

func NewTextTranslateBatchResponse() (response *TextTranslateBatchResponse) {
	response = &TextTranslateBatchResponse{
		BaseResponse: &tchttp.BaseResponse{},
	}
	return

}

func NewClient(credential common.CredentialIface, region string, clientProfile *profile.ClientProfile) (client *Client, err error) {
	client = &Client{}
	client.Init(region).
		WithCredential(credential).
		WithProfile(clientProfile)
	return
}
func (c *Client) TextTranslateBatch(request *TextTranslateBatchRequest) (response *TextTranslateBatchResponse, err error) {
	return c.TextTranslateBatchWithContext(context.Background(), request)
}
func (c *Client) TextTranslateBatchWithContext(ctx context.Context, request *TextTranslateBatchRequest) (response *TextTranslateBatchResponse, err error) {
	if request == nil {
		request = NewTextTranslateBatchRequest()
	}

	if c.GetCredential() == nil {
		return nil, errors.New("TextTranslateBatch require credential")
	}

	request.SetContext(ctx)

	response = NewTextTranslateBatchResponse()
	err = c.Send(request, response)
	return
}

func UseDefaultEN2ZH(zhContent []string) (targetList []string) {
	if TencentClient == nil {
		return
	}

	source := "en"
	target := "zh"
	var projectId int64 = 0
	request := NewTextTranslateBatchRequest()
	request.Source = &source
	request.Target = &target
	request.ProjectId = &projectId

	sourceList := make([]*string, len(zhContent))
	for i, c := range zhContent {
		copyC := c
		sourceList[i] = &copyC
	}
	request.SourceTextList = sourceList

	batchResponse, err := TencentClient.TextTranslateBatch(request)
	if err != nil || batchResponse.Response == nil || batchResponse.Response.TargetTextList == nil {
		LogF("TextTranslateBatch error %v", err)
		return
	}

	targetTextList := batchResponse.Response.TargetTextList
	for _, c := range targetTextList {
		targetList = append(targetList, *c)
	}
	return
}
