package main

import (
	"bytes"
	"log"
	"testing"
	"time"
)

func Test_generateHtmlMessage(t *testing.T) {
	InitDB()
	InitConfig()

	var buf bytes.Buffer
	err := NewGenHtml(false).writeHtmlContentData(time.Now().Format(SimpleTimeFormatStr), &buf)
	if err != nil {
		t.Errorf("generate error: %v", err)
		return
	}

	log.Printf("generate html is %v", buf.String())
}
