package main

import (
	_ "embed"
	"log"
	"sort"

	"github.com/goccy/go-yaml"
)

type (
	ProjectConfig struct {
		Feeds   []Feed        `yaml:"feeds"`
		Cron    CronConfig    `yaml:"cron"`
		Tencent TencentConfig `yaml:"tencent"`
		Mail    *MailConfig   `yaml:"mail"`
		MainTo  []string      `yaml:"mailto"`
		Log     Log           `yaml:"log"`
	}

	Log struct {
		OpenConsole bool `yaml:"openConsole"`
	}

	Feed struct {
		Name   string `yaml:"name"`
		URL    string `yaml:"url"`
		Weight int    `yaml:"weight"`
	}

	CronConfig struct {
		Rss  string `yaml:"rss"`
		Mail string `yaml:"mail"`
	}

	MailConfig struct {
		Smtp     string `yaml:"smtp"`
		Port     int    `yaml:"port"`
		User     string `yaml:"user"`
		Password string `yaml:"password"`
	}

	TencentConfig struct {
		Enable    bool   `yaml:"enable"`
		SecretId  string `yaml:"secretId"`
		SecretKey string `yaml:"secretKey"`
	}
)

//go:embed config.yaml
var configBlob []byte

//go:embed config-feed.yaml
var configExampleBlob []byte

var (
	GlobalConfig ProjectConfig
)

func InitConfig() {
	log.Printf("Init Config start")

	// 解析示例配置和实际配置
	var exampleCf, actualCf ProjectConfig
	if err := yaml.Unmarshal(configExampleBlob, &exampleCf); err != nil {
		log.Fatalf("Failed to parse example config: %v", err)
	}
	if err := yaml.Unmarshal(configBlob, &actualCf); err != nil {
		log.Fatalf("Failed to parse actual config: %v", err)
	}

	actualCf.Feeds = exampleCf.Feeds

	// 按权重排序feeds
	sort.Slice(actualCf.Feeds, func(i, j int) bool {
		return actualCf.Feeds[i].Weight > actualCf.Feeds[j].Weight
	})

	GlobalConfig = actualCf

	log.Printf("Init Config end")
}
