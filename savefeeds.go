package main

import (
	"crypto/tls"
	"encoding/json"
	"feed2mail/ent"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/mmcdole/gofeed"
)

type SaveFeedContentTask struct {
	fp          *gofeed.Parser
	hc          http.Client
	concurrency int
}

func SaveFeeds() {
	sfct := SaveFeedContentTask{
		fp: gofeed.NewParser(),
		hc: http.Client{
			Timeout: 10 * time.Second,
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
			},
		},
	}
	sfct.SaveFeedContentRunnable()
}

func (sfct *SaveFeedContentTask) SaveFeedContentRunnable() {
	_ = AddLogs("SaveFeedContent start.", true)

	feeds, err := FeedsTable.GetAll()
	if err != nil {
		_ = AddLogs(fmt.Sprintf("SaveFeedContent query feeds error: %v", err), false)
		return
	}

	wg := sync.WaitGroup{}
	ch := make(chan bool, sfct.concurrency)
	for _, feed := range feeds {
		feedUrl, feedName := feed.FeedURL, feed.FeedName
		if feedName == "" || feedUrl == "" {
			_ = AddLogs(fmt.Sprintf("SaveFeedContent error: bad feed info %v", feed), false)
			continue
		}

		if sfct.concurrency > 1 {
			ch <- true
			wg.Add(1)
			go sfct.SaveAndLog(feedName, feedUrl, func() {
				wg.Done()
				<-ch
			})
		} else {
			sfct.SaveAndLog(feedName, feedUrl, func() {})
		}
	}

	wg.Wait()
	close(ch)

	_ = AddLogs("get all rss done", true)
}

func (sfct *SaveFeedContentTask) SaveAndLog(feedName string, feedUrl string, deferFunc func()) {
	defer func() {
		deferFunc()
		if unknow := recover(); unknow != nil {
			_ = AddLogs(fmt.Sprintf("【%s】: get rss error, recover is %v", feedName, unknow), false)
		}
	}()

	_ = AddLogs(fmt.Sprintf("【%s】: start get rss", feedName), true)

	newSize, err := sfct.SaveFeedFromRss(feedUrl, feedName)
	if err != nil {
		_ = AddLogs(fmt.Sprintf("【%s】: get rss error, err is %v", feedName, err), false)
		return
	}
	_ = AddLogs(fmt.Sprintf("【%s】: get rss success, save size %d", feedName, newSize), true)
}

func (sfct *SaveFeedContentTask) SaveFeedFromRss(feedUrl, feedName string) (int, error) {
	resp, err := sfct.hc.Get(feedUrl)
	if err != nil {
		return 0, err
	}

	body := resp.Body
	defer func() { _ = body.Close() }()

	gfeed, err := sfct.fp.Parse(body)
	if err != nil {
		return 0, err
	}

	items := gfeed.Items

	bs, err := json.Marshal(gfeed)
	if err != nil {
		_ = AddLogs(fmt.Sprintf("【%s】:parse success, gfeed %s", feedName, string(bs)), true)
	}

	return sfct.SaveFeedContent(items, feedName)
}

func (sfct *SaveFeedContentTask) SaveFeedContent(items []*gofeed.Item, feedName string) (int, error) {
	newLinks := []string{}
	for _, item := range items {
		newLinks = append(newLinks, item.Link)
	}

	existLinks, err := FeedContentTable.QueryExistInDBLinks(feedName, newLinks)
	if err != nil {
		return 0, err
	}

	var fc []*ent.FeedContent
	for _, item := range items {
		if IsContain(existLinks, item.Link) {
			continue
		}

		_ = AddLogs(fmt.Sprintf("【%s】: add link: %s, title: %s", feedName, item.Link, item.Title), true)

		now := time.Now()
		fc = append(
			fc,
			&ent.FeedContent{
				FeedName:      feedName,
				Link:          item.Link,
				Title:         item.Title,
				Detail:        item.Description,
				CreateTime:    now,
				AtDay:         now.Format(SimpleTimeFormatStr),
				PublishedTime: item.PublishedParsed,
			},
		)
	}

	sfct.translate(fc)

	if len(fc) > 0 {
		FeedContentTable.BatchSaveFeedContent(fc)
		FeedsTable.UpdateNewContentTime(feedName, time.Now())
		return len(fc), nil
	}

	return 0, nil
}

func (sfct *SaveFeedContentTask) translate(fc []*ent.FeedContent) {
	enToZh := make(map[string]string)
	var enTitles []string

	// 收集英文标题并构建映射
	for _, f := range fc {
		if IsMostlyEnglish(f.Title) {
			enTitles = append(enTitles, f.Title)
		}
	}

	if len(enTitles) == 0 {
		return
	}

	zhTitles := UseDefaultEN2ZH(enTitles)
	for i, enT := range enTitles {
		if i < len(zhTitles) {
			enToZh[enT] = zhTitles[i]
		}
	}

	// 更新标题
	for i := range fc {
		if zhTitle, exists := enToZh[fc[i].Title]; exists {
			fc[i].Title = fmt.Sprintf("%s(%s)", fc[i].Title, zhTitle)
		}
	}
}

func AddLogs(content string, success bool) error {
	if !success {
		LogF(content)
	}

	return FeedLogTable.AddLogs(content, success)
}
