package main

import (
	"bytes"
	"embed"
	"feed2mail/ent"
	"feed2mail/ent/feedlog"
	"log"
	"net/http"
	"text/template"
	"time"

	_ "github.com/mattn/go-sqlite3"
	"github.com/robfig/cron/v3"
)

//go:embed templates/*.html
var ts embed.FS

func main() {
	InitConfig()
	InitDB()
	InitFeeds()
	InitLog("log.log", "FEED2MAIL", GlobalConfig.Log.OpenConsole)
	if GlobalConfig.Tencent.Enable {
		InitTencentClient()
	}

	taskStart()

	HttpServerStart()
}

func InitFeeds() {
	feeds := GlobalConfig.Feeds

	allFeeds, err := FeedsTable.GetAll()
	if err != nil {
		panic(err)
	}

	var feedList []*ent.Feeds
	for _, feed := range feeds {
		var tempOldFeed *ent.Feeds
		for _, oldFeed := range allFeeds {
			if oldFeed.FeedName == feed.Name {
				tempOldFeed = oldFeed
			}
		}

		newContentTime := time.Now()
		if tempOldFeed != nil {
			newContentTime = tempOldFeed.NewContentTime
		}

		feedList = append(feedList, &ent.Feeds{
			FeedName:       feed.Name,
			FeedURL:        feed.URL,
			FeedWeight:     feed.Weight,
			CreateTime:     time.Now(),
			NewContentTime: newContentTime,
		})
	}

	FeedsTable.Clean()
	FeedsTable.InsertList(feedList)
}

func HttpServerStart() {
	parseFS, parseErr := template.ParseFS(ts, "templates/*.html")
	if parseErr != nil {
		panic(parseErr)
	}

	http.Handle("/", http.RedirectHandler("/content", http.StatusMovedPermanently))

	http.HandleFunc("/config", func(resp http.ResponseWriter, request *http.Request) {
		data := make(map[string]any)
		data["RssCron"] = GlobalConfig.Cron.Rss

		allFeeds, err := FeedsTable.GetAllForView()
		if err != nil {
			LogF("config query error: %v", err)
		}
		data["Feeds"] = allFeeds

		_ = parseFS.ExecuteTemplate(resp, "config.html", data)
	})

	http.HandleFunc("/content", func(resp http.ResponseWriter, request *http.Request) {
		resp.Header().Set("Content-Type", "text/html")
		dateStr := request.FormValue("date")
		if dateStr == "" {
			dateStr = time.Now().Format(SimpleTimeFormatStr)
		}
		err := NewGenHtml(false).writeHtmlContentData(dateStr, resp)
		if err == nil {
			return
		}

		_ = parseFS.ExecuteTemplate(resp, "exception.html", err)
	})

	http.HandleFunc("/log", func(resp http.ResponseWriter, request *http.Request) {
		logType := request.FormValue("type")
		if logType == "" {
			logType = feedlog.LogTypeALL.String()
		}
		queryTypeInt := feedlog.LogType(logType)
		list, err := FeedLogTable.LogList(queryTypeInt)

		if err != nil {
			_ = parseFS.ExecuteTemplate(resp, "exception.html", err)
			return
		}

		_ = parseFS.ExecuteTemplate(resp, "log.html", list)
	})

	log.Panicln(http.ListenAndServe(":9000", nil))
}

func taskStart() {
	secondParser := cron.NewParser(
		cron.Second | cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.DowOptional | cron.Descriptor,
	)
	c := cron.New(cron.WithParser(secondParser), cron.WithChain())

	// 保存Feed内容
	rssCron := getOrDefault(GlobalConfig.Cron.Rss, "0 0 0/1 * * ?")
	addCronTask(c, rssCron, SaveFeeds, "保存Feed内容")

	// 数据清理
	addCronTask(c, "0 10 0 * * ?", dataClean, "数据清理")

	// 发送邮件
	mailCron := getOrDefault(GlobalConfig.Cron.Mail, "0 0 8 * * ?")
	addCronTask(c, mailCron, sendDailyEmail, "发送每日邮件")

	c.Start()
}

func addCronTask(c *cron.Cron, schedule string, task func(), description string) {
	entryID, err := c.AddFunc(schedule, task)
	if err != nil {
		LogF("添加任务 '%s' 失败: %v", description, err)
	} else {
		LogF("成功添加任务 '%s', ID: %v", description, entryID)
	}
}

func sendDailyEmail() {
	var buf bytes.Buffer
	err := NewGenHtml(true).writeHtmlContentData(time.Now().Add(-24*time.Hour).Format(SimpleTimeFormatStr), &buf)
	if err != nil {
		LogF("生成HTML内容错误: %v", err)
		return
	}

	sender := NewMailSender(GlobalConfig.Mail.Smtp, GlobalConfig.Mail.Port, GlobalConfig.Mail.User, GlobalConfig.Mail.Password)
	for _, mailto := range GlobalConfig.MainTo {
		err := sender.SendEmail("Feed2Mail", mailto, "RSS每日摘要", buf.String())
		LogF("发送邮件到 %v 结果: %v", mailto, err)
	}
}

func dataClean() {
	feedMaxDate := time.Now().AddDate(-1, 0, 0)
	_, err := FeedContentTable.CleanContents(feedMaxDate)
	if err != nil {
		LogF("CleanContent= err: %v", err)
	}

	logMaxDate := time.Now().AddDate(0, 0, -7)
	_, err = FeedLogTable.CleanLogs(logMaxDate)
	if err != nil {
		LogF("CleanLogs err: %v", err)
	}
}

func getOrDefault(v, d string) string {
	if v == "" {
		return d
	}
	return v
}
