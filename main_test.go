package main

import (
	"crypto/tls"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/mmcdole/gofeed"
)

func setup() {
	InitLog("log.log", "FEED2MAIL_TEST", true)
	InitConfig()
	InitDB()
	InitFeeds()
	InitTencentClient()
}

func TestSaveFeedContentTask(t *testing.T) {
	os.Remove(GetDBPath())

	setup()

	sfct := SaveFeedContentTask{
		fp: gofeed.NewParser(),
		hc: http.Client{
			Timeout: 10 * time.Second,
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{InsecureSkipVerify: true}},
		},
		concurrency: 1,
	}
	sfct.SaveFeedContentRunnable()

	// os.Remove(GetDBPath())
}

func Test_dataClean(t *testing.T) {
	setup()

	dataClean()
}
