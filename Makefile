.PHONY: build clean

BIN_NAME = app

all: clean build

build:
	CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build -o ${BIN_NAME}

clean:
	rm -rf ${BIN_NAME}
	go clean -i .
