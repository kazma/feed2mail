# Feed2mail

抓取RSS，并且支持按天查看抓取的数据

## 特性

1. RSS定时抓取
2. YML配置RSS源，支持配置权重以对RSS排序，权重值低于50不展示详情
3. 支持配置腾讯云翻译英文RSS源的标题

## 启动

1. 参考config-example.yml，完善一份config.yml
    - `cp config-example.yml config.yml`
2. 安装`docker`和`docker-compose`
3. 启动`docker compose -f ./deploy/docker-compose.yml up for_local -d --build`
4. 停止`docker compose -f ./deploy/docker-compose.yml down for_local -v`
5. for_server版本是需要本地build app：`CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build -o app`