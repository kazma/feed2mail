package main

import (
	"context"
	_ "embed"
	"feed2mail/ent"
	"feed2mail/ent/feedcontent"
	"feed2mail/ent/feedlog"
	"feed2mail/ent/feeds"
	"log"
	"os"
	"path/filepath"
	"time"

	"entgo.io/ent/dialect/sql"
	"github.com/pkg/errors"
)

type (
	Table struct {
		TableName string
	}

	FeedsT struct {
		Table
	}

	FeedContentT struct {
		Table
	}

	FeedLogT struct {
		Table
	}
)

var (
	entClient  *ent.Client
	FeedsTable = &FeedsT{
		Table{
			TableName: "feeds",
		},
	}

	FeedContentTable = &FeedContentT{
		Table{
			TableName: "feed_contents",
		},
	}

	FeedLogTable = &FeedLogT{
		Table{
			TableName: "feed_logs",
		},
	}
)

func InitDB() {
	dbPath := GetDBPath()

	base := filepath.Dir(dbPath)
	_, err := os.Open(base)
	if errors.Is(err, os.ErrNotExist) {
		err = os.MkdirAll(base, os.ModeDir)
		if err != nil {
			panic(err)
		}
	}

	log.Printf("Init DB start. Path is %s", dbPath)

	_entClient, err := ent.Open("sqlite3", "file:"+dbPath+"?cache=shared&_fk=1")
	if err != nil {
		panic(err)
	}

	if err := _entClient.Schema.Create(context.Background()); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}

	entClient = _entClient

	log.Printf("Init DB end")
}

func GetDBPath() string {
	dbPath := "./db/data.db"
	if p, success := os.LookupEnv("FM_DB_PATH"); success {
		dbPath = p
	}
	return dbPath
}

// feeds

func (*FeedsT) InsertList(feeds []*ent.Feeds) error {
	if len(feeds) <= 0 {
		return nil
	}

	fs := []*ent.FeedsCreate{}
	for _, feed := range feeds {
		f := entClient.Feeds.Create().
			SetFeedName(feed.FeedName).
			SetFeedURL(feed.FeedURL).
			SetFeedWeight(feed.FeedWeight).
			SetCreateTime(feed.CreateTime).
			SetNewContentTime(feed.NewContentTime)
		fs = append(fs, f)
	}

	_, err := entClient.Feeds.CreateBulk(fs...).Save(context.Background())

	return err
}

func (*FeedsT) Clean() error {
	_, err := entClient.Feeds.Delete().Exec(context.Background())
	return err
}

func (*FeedsT) GetAll() (feeds []*ent.Feeds, err error) {
	feeds, err = entClient.Feeds.Query().All(context.Background())
	return
}

func (*FeedsT) GetAllForView() (fs []*ent.Feeds, err error) {
	fs, err = entClient.Feeds.Query().Order(feeds.ByNewContentTime(sql.OrderDesc())).All(context.Background())
	return
}

func (*FeedsT) UpdateNewContentTime(feedName string, date time.Time) error {
	return entClient.Feeds.Update().
		SetNewContentTime(date).
		Where(feeds.FeedNameEQ(feedName)).
		Exec(context.Background())
}

// feed_contents

func (*FeedContentT) BatchSaveFeedContent(fc []*ent.FeedContent) error {
	if len(fc) <= 0 {
		return nil
	}

	fcs := make([]*ent.FeedContentCreate, len(fc))
	for i, f := range fc {
		pt := f.PublishedTime
		if pt == nil {
			pt = &time.Time{}
		}
		fcs[i] = entClient.FeedContent.Create().
			SetFeedName(f.FeedName).
			SetLink(f.Link).
			SetTitle(f.Title).
			SetDetail(f.Detail).
			SetCreateTime(f.CreateTime).
			SetAtDay(f.AtDay).
			SetPublishedTime(*pt)
	}

	_, err := entClient.FeedContent.CreateBulk(fcs...).Save(context.Background())
	return err
}

func (*FeedContentT) QueryExistInDBLinks(name string, links []string) ([]string, error) {
	if len(links) == 0 {
		return []string{}, nil
	}

	var existLinks []string
	query, err := entClient.FeedContent.Query().
		Where(feedcontent.FeedNameEQ(name)).
		Where(feedcontent.LinkIn(links...)).
		Select(feedcontent.FieldLink).
		All(context.Background())
	if err != nil {
		return nil, err
	}

	for _, q := range query {
		existLinks = append(existLinks, q.Link)
	}

	return existLinks, nil
}

func (*FeedContentT) QueryFeedContents(name string, dateStr string) (feedContents []*ent.FeedContent, err error) {
	feedContents, err = entClient.FeedContent.Query().
		Where(feedcontent.FeedNameEQ(name)).
		Where(feedcontent.AtDayEQ(dateStr)).
		Order(feedcontent.ByPublishedTime(sql.OrderDesc())).
		All(context.Background())
	return
}

func (*FeedContentT) CleanContents(date time.Time) (int, error) {
	return entClient.FeedContent.Delete().
		Where(feedcontent.CreateTimeLT(date)).
		Exec(context.Background())
}

// logs

func (*FeedLogT) AddLogs(content string, success bool) error {
	n := time.Now()
	state := feedlog.LogTypeALL
	if !success {
		state = feedlog.LogTypeERROR
	}

	_, err := entClient.FeedLog.Create().
		SetContent(content).
		SetCreateTime(n).
		SetLogType(state).
		Save(context.Background())
	return err
}

func (*FeedLogT) LogList(queryType feedlog.LogType) (feedLogs []*ent.FeedLog, err error) {

	query := entClient.FeedLog.Query().
		Order(feedlog.ByCreateTime(sql.OrderDesc()))

	if queryType != feedlog.LogTypeALL {
		query.Where(feedlog.LogTypeEQ(queryType))
	}

	feedLogs, err = query.All(context.Background())
	return
}

func (*FeedLogT) CleanLogs(maxDate time.Time) (int, error) {
	return entClient.FeedLog.Delete().
		Where(feedlog.CreateTimeLT(maxDate)).
		Exec(context.Background())
}
