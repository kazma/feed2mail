package main

import (
	"io"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"gopkg.in/natefinch/lumberjack.v2"
)

var (
	DefaultZone         = time.FixedZone("UTC+8", 0)
	TimeFormatStr       = "2006-01-02 15:04:05"
	SimpleTimeFormatStr = "2006-01-02"

	regExp = regexp.MustCompile(`[a-zA-Z.,!?\\-\\s]+`)
)

var logx log.Logger

func Time2String(t *time.Time, format string) string {
	if t == nil {
		return time.Now().Format(format)
	}

	return t.Local().Format(format)
}

func IsContain(list []string, val string) bool {
	for _, el := range list {
		if strings.EqualFold(el, val) {
			return true
		}
	}

	return false
}

func IsMostlyEnglish(text string) bool {
	matches := regExp.FindAllString(text, -1)
	matchedChars := 0
	for _, match := range matches {
		matchedChars += len(match)
	}

	return matchedChars > len(text)/2
}

func InitLog(logPath string, prefix string, openConsole bool) {
	base := filepath.Dir(logPath)
	_, err := os.Stat(base)
	if err != nil {
		if os.IsNotExist(err) {
			err = os.MkdirAll(base, os.ModeAppend)
			if err != nil {
				panic(err)
			}
		} else {
			panic(err)
		}
	}

	logFile := &lumberjack.Logger{
		Filename:   logPath,
		MaxBackups: 7,
		MaxAge:     7,
		Compress:   true,
	}

	if openConsole {
		multiWriter := io.MultiWriter(os.Stdout, logFile)
		logx.SetOutput(multiWriter)
	} else {
		logx.SetOutput(logFile)
	}
	logx.SetFlags(log.Ldate | log.Ltime | log.Llongfile)
	logx.SetPrefix("[" + prefix + "] ")
}

func LogF(format string, args ...interface{}) {
	if args != nil {
		logx.Printf(format, args...)
		return
	}
	logx.Println(format)
}
