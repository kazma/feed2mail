package main

import (
	"testing"
)

func TestNewMailSender(t *testing.T) {
	InitConfig()
	config := GlobalConfig
	mail := NewMailSender(config.Mail.Smtp, config.Mail.Port, config.Mail.User, config.Mail.Password)
	err := mail.SendEmail("Feed2Mail 测试", "xxx@outlook.com", " 测试", " 测试")
	if err != nil {
		t.Errorf("SendEmail() error = %v", err)
	}
}
