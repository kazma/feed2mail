package main

import (
	"feed2mail/ent/feedlog"
	"testing"
	"time"
)

func TestCleanLogs(t *testing.T) {
	InitDB()

	err := FeedLogTable.AddLogs("ok", true)
	if err != nil {
		t.Errorf("AddLogs() error = %v", err)
		return
	}

	list, err := FeedLogTable.LogList(feedlog.LogTypeERROR)
	if err != nil {
		t.Errorf("LogList() error = %v", err)
		return
	}

	t.Logf("log %v", list)

	logMaxDate := time.Now().AddDate(0, 0, -7)
	_, err = FeedLogTable.CleanLogs(logMaxDate)
	if err != nil {
		t.Errorf("CleanLogs() error = %v", err)
	}

	list, err = FeedLogTable.LogList(feedlog.LogTypeALL)
	if err != nil {
		t.Errorf("LogList() later error = %v", err)
		return
	}

	t.Logf("log later %v", list)

	FeedLogTable.AddLogs("ok1", true)
	FeedLogTable.AddLogs("ok2", true)
	FeedLogTable.AddLogs("ok3", true)
	FeedLogTable.AddLogs("ok4", false)
	FeedLogTable.AddLogs("ok1", true)
	FeedLogTable.AddLogs("ok5", false)
}
