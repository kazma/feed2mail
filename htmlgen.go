package main

import (
	"bytes"
	"errors"
	"io"
	"log"
	"text/template"
	"time"

	"github.com/PuerkitoBio/goquery"
)

type (
	GenHtml struct {
		baseWidget int
		mail       bool
	}
)

type (
	HtmlData struct {
		CurrentTime     string
		FeedSize        int
		ContentSize     int
		Content         []HtmlContent
		FeedDate        string
		YesterdayString string
		Mail            bool
	}

	HtmlContent struct {
		Link  string
		Title string
		Count int
		Item  []HtmlItem
	}

	HtmlItem struct {
		Link        string
		Title       string
		Description string
		PublishTime time.Time
		CreateTime  time.Time
	}
)

var (
	ErrNoContent = errors.New("no content")
)

func NewGenHtml(mail bool) GenHtml {
	return GenHtml{
		baseWidget: 50,
		mail:       mail,
	}
}

func (gh GenHtml) queryFeedContent(dateStr string) (data HtmlData, err error) {
	feeds, err := FeedsTable.GetAll()
	if err != nil {
		return HtmlData{}, err
	}

	var contents []HtmlContent
	for _, f := range feeds {
		feedContents, err := FeedContentTable.QueryFeedContents(f.FeedName, dateStr)
		if err != nil {
			return HtmlData{}, err
		}

		if len(feedContents) == 0 {
			continue
		}

		var mailItems []HtmlItem
		for _, fc := range feedContents {
			pt := fc.PublishedTime
			if pt == nil {
				pt = &time.Time{}
			}
			mailItems = append(mailItems, HtmlItem{
				Link:        fc.Link,
				Title:       fc.Title,
				PublishTime: *pt,
				CreateTime:  fc.CreateTime,
				Description: gh.getDescription(fc.Detail, f.FeedWeight),
			},
			)
		}

		contents = append(
			contents,
			HtmlContent{Link: f.FeedURL, Title: f.FeedName, Item: mailItems, Count: len(mailItems)},
		)

		data.FeedSize += 1
		data.ContentSize += len(mailItems)
	}

	data.CurrentTime = time.Now().Format(TimeFormatStr)
	data.Content = contents
	data.FeedDate = dateStr
	data.Mail = gh.mail

	if dateStr == "" {
		data.YesterdayString = time.Now().AddDate(0, 0, -1).Format("2006-01-02")
	} else {
		queryDate, err := time.Parse("2006-01-02", dateStr)
		if err == nil {
			data.YesterdayString = queryDate.AddDate(0, 0, -1).Format("2006-01-02")
		}
	}

	return
}

func (gh GenHtml) writeHtmlContentData(dateStr string, wr io.Writer) (err error) {
	mailData, err := gh.queryFeedContent(dateStr)
	if err != nil {
		log.Printf("queryFeedContent error %v", err)
		return
	}

	t, err := template.ParseFiles("templates/content.html")
	if err != nil {
		return
	}

	// render html
	err = t.Execute(wr, mailData)

	return
}

func (gh GenHtml) getDescription(content string, widget int) string {
	if widget < gh.baseWidget {
		return ""
	}

	if len(content) <= 250 {
		return content
	}

	// remove html tag
	h, err := goquery.NewDocumentFromReader(bytes.NewBufferString(content))
	if err != nil {
		log.Printf("parse html error %v", err)
		return content
	} else {
		t := h.Text()
		des := []rune(t)
		// less 200 word
		if len(des) > 200 {
			return string(des[:200])
		}
		return t
	}
}
