package main

import (
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/profile"
	"testing"
)

func TestClient_TextTranslateBatch(t *testing.T) {
	InitConfig()

	client, err := NewClient(
		common.NewCredential(GlobalConfig.Tencent.SecretId, GlobalConfig.Tencent.SecretKey),
		"ap-guangzhou",
		profile.NewClientProfile(),
	)
	if err != nil {
		panic(err)
	}

	source := "auto"
	target := "zh"
	var projectId int64 = 0
	request := NewTextTranslateBatchRequest()
	request.Source = &source
	request.Target = &target
	request.ProjectId = &projectId

	sourceText := "Hello"
	request.SourceTextList = []*string{
		&sourceText,
	}

	batchResponse, err := client.TextTranslateBatch(request)
	if err != nil {
		panic(err)
	}

	t.Logf("%v", batchResponse.ToJsonString())
}
